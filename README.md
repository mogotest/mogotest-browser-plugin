# Introduction #

This is a browser plugin for Safari and Chrome on Windows.  It was used at one time as part of the Mogotest Web Consistency Testing service, allowing for full-canvas screenshots in those browsers.
Mogotest stopped using this plugin after Apple abandoned Safari on Windows and Chrome modified its rendering engine in a way that broke the technique used here for capturing the full canvas.
At this point, the plugin should be used for little more than an example of how to create a FireBreath plugin.  However, the plugin is using a rather old version of FireBreath and may
even fail to be useful on that account.

The code is licensed under the Apache License, version 2.0.  This project is no longer maintained and should probably not be used without at least synchronizing with the latest version of FireBreath.

# Compiling the Mogotest Browser Plugin #

## FireBreath ##

We use [FireBreath](http://firebreath.googlecode.com/) as the framework for developing our framework. FireBreath lays out a good foundation, but we have had to customize a few things. The subsections describe that.

### Prerequisites ###

In order to build the Mogotest Browser Plugin you will need the following:

* Windows computer
* git
* Visual Studio 2010

### Source Structure ###

To build the project you need two different source repositories: ours and FireBreath's.

```
#!bash


git clone git@bitbucket.org:mogotest/mogotest-browser-plugin.git
```


Our project only contains our project files. To build the project, however, we need a set of generated files from FireBreath. To do so, we use FireBreath's prep command from within the FireBreath source tree:


```
#!bash

cd firebreath
.\prep2010.cmd ..\projects ..\build
```


Once that's completed, you can open up the generated `FireBreath.sln` file from the `..\mogotest-browser-plugin\build` directory.

### Boost ###

***FireBreath now ships with a recent version of boost, so we don't need to compile it manually any longer. The instructions below are for historical purposes only.***

FireBreath bundles a subset of the [boost library](http://boost.org/) to help make some tasks easier. If they don't supply a component that we need, we have to repackage boost.

To bundle the subset, we first need a source tree.  As of 1.43.0, boost does not compile properly with VS 2010.  This should be fixed in 1.44.0, but until then, you'll need to grab trunk.  Once you've got a source tree, you need to compile the [bcp](http://www.boost.org/doc/libs/1_40_0/tools/bcp/bcp.html) tool:


```
#!bash

cd ${BOOST_ROOT}\tools\bcp
..\..\bjam.exe
cd ..\..\   [Should be back at ${BOOST_ROOT} again]
cp bin.v2\tools\bcp\msvc-10.0\release\link-static\threading-multi\bcp.exe .
```


`bcp` is what we'll be using to extract the subset of boost needed by FireBreath and by our plugin. To use it, we'll execute the following commands:


```
#!bash

#################################################
# Extract the components of boost that we want. #
#################################################

cd ${BOOST_ROOT}
rm -recurse -force firebreath [This will error out if the directory doesn't exist]
mkdir firebreath
.\bcp.exe boost/mpl/contains.hpp boost/type_traits.hpp function_types thread assign boost/preprocessor/list/for_each_i.hpp format firebreath\


#########################################
# Build the necessary static libraries. #
#########################################

cd firebreath
..\bjam.exe --with-date_time --with-thread runtime-link=static link=static


##############################################
# Move everything to our plugin source tree. #
##############################################
rm -recurse -force ${FIREBREATH_ROOT}\src\3rdParty\boost
mkdir ${FIREBREATH_ROOT}\src\3rdParty\boost

mv boost ${FIREBREATH_ROOT}\src\3rdParty\boost
mv stage\lib ${FIREBREATH_ROOT}\src\3rdParty\boost\lib
```

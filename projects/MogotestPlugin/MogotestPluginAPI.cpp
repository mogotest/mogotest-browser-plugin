/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"

#include "MogotestPluginAPI.h"
#include "BrowserManager.h"
#include "Win/PluginWindowWin.h"

#include "boost/Thread.hpp"
#include <boost/algorithm/string.hpp>

#include <atlimage.h>

///////////////////////////////////////////////////////////////////////////////
/// @fn MogotestPluginAPI::MogotestPluginAPI(const MogotestPluginPtr& plugin, const FB::BrowserHostPtr host)
///
/// @brief  Constructor for your JSAPI object.  You should register your methods, properties, and events
///         that should be accessible to Javascript from here.
///
/// @see FB::JSAPIAuto::registerMethod
/// @see FB::JSAPIAuto::registerProperty
/// @see FB::JSAPIAuto::registerEvent
///////////////////////////////////////////////////////////////////////////////
MogotestPluginAPI::MogotestPluginAPI(const MogotestPluginPtr& plugin, const FB::BrowserHostPtr& host) : m_plugin(plugin), m_host(host)
{
  this->logger = new Logger(host);

  registerMethod("captureEntirePageScreenshot", make_method(this, &MogotestPluginAPI::captureEntirePageScreenshot));

  // Read-only property
  registerProperty("version",
    make_property(this,
    &MogotestPluginAPI::get_version));
}

///////////////////////////////////////////////////////////////////////////////
/// @fn MogotestPluginAPI::~MogotestPluginAPI()
///
/// @brief  Destructor.  Remember that this object will not be released until
///         the browser is done with it; this will almost definitely be after
///         the plugin is released.
///////////////////////////////////////////////////////////////////////////////
MogotestPluginAPI::~MogotestPluginAPI()
{
  if (NULL != logger)
  {
    delete logger;
    logger = NULL;
  }
}

///////////////////////////////////////////////////////////////////////////////
/// @fn MogotestPluginPtr MogotestPluginAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
MogotestPluginPtr MogotestPluginAPI::getPlugin()
{
  MogotestPluginPtr plugin(m_plugin.lock());
  if (!plugin)
  {
    throw FB::script_error("The plugin is invalid");
  }

  return plugin;
}

// Read-only property version
std::string MogotestPluginAPI::get_version()
{
  return FBSTRING_PLUGIN_VERSION;
}

// Taken from the Selenium Chrome plugin.
HWND MogotestPluginAPI::getRenderWindow(const wchar_t* windowName)
{
  FB::PluginWindowWin* window = (FB::PluginWindowWin*) getPlugin()->GetWindow();
  HWND hwnd = window->getHWND();

  wchar_t *window_class_name = new wchar_t[wcslen(windowName) + 1];

  while (hwnd != NULL)
  {
    GetClassName(hwnd, window_class_name, wcslen(windowName) + 1);

    if (!wmemcmp(window_class_name, windowName, wcslen(windowName))) {
      break;
    }

    hwnd = GetParent(hwnd);
  }

  return hwnd;
}

HWND MogotestPluginAPI::getBrowserWindow()
{
	FB::PluginWindowWin* window = (FB::PluginWindowWin*) getPlugin()->GetWindow();
	return GetAncestor(window->getHWND(), GA_ROOTOWNER);
}

void threadFunction(boost::shared_ptr<MogotestPluginAPI> api, const std::string filename, const std::string userAgent, const FB::JSObjectPtr callback)
{
  api->captureEntirePageScreenshotThread(filename, userAgent, callback);
}

void MogotestPluginAPI::captureEntirePageScreenshot(const FB::variant& filename, const FB::JSObjectPtr callback)
{
  std::string convertedFilename = filename.cast<std::string>();

  boost::shared_ptr<FB::Npapi::NpapiBrowserHost> nativeBrowser = FB::ptr_cast<FB::Npapi::NpapiBrowserHost>(m_host);
  std::string userAgent(nativeBrowser->UserAgent());

  boost::thread make_thread(threadFunction, FB::ptr_cast<MogotestPluginAPI>(shared_from_this()), convertedFilename, userAgent, callback);
}

void MogotestPluginAPI::captureEntirePageScreenshotThread(const std::string filename, const std::string userAgent, const FB::JSObjectPtr callback)
{
  HWND browser = getBrowserWindow();
  HWND canvas = NULL;

  // Check the user agent to see what browser we're running in.  This is necessary because each browser identifies its tab window
  // differently and we need to access it in order to do our screenshotting.
  if (boost::algorithm::contains(userAgent, "Chrome"))
  {
	canvas = getRenderWindow(L"Chrome_RenderWidgetHostHWND");
  }
  
  // Make sure we check for Safari after Chrome, since Chrome reports itself as being Safari compatible.
  else if(boost::algorithm::contains(userAgent, "Safari"))
  {
	canvas = getRenderWindow(L"WebKit2WebViewWindowClass");
  }

  BrowserManager* browserManager = new BrowserManager(browser, canvas, m_host, logger);
  browserManager->maximize();
  
  int width = this->m_host->getDOMDocument()->getBody()->getScrollWidth();
  int height = this->m_host->getDOMDocument()->getBody()->getScrollHeight();

  logger->info(str(boost::format("Screenshot height: %d") % height));

  CImage image;
  image.Create(width, height, 24);
  CImageDC imageDC(image);

  logger->info("Taking screenshot");

  // Capture the screenshot to an internal memory buffer.
  HRESULT hr = PrintWindow(canvas, imageDC, NULL);
  if (FAILED(hr))
  {
    logger->win32Error("PrintWindow");
  }

  browserManager->restore();
  delete browserManager;
  
  // Save the screenshot.  Since the save can take a little while, we save to a temp file
  // first and then rename the temp file to the correct filename.  This limits the likelihood
  // the calling process tries to read the file before the write has been completed.
  std::wstring convertedFilename = FB::utf8_to_wstring(filename);
  convertedFilename.append(L".tmp");
  hr = image.Save(convertedFilename.c_str(), Gdiplus::ImageFormatPNG);
  if (FAILED(hr))
  {
    logger->win32Error("Image save");
  }
  
  MoveFile(convertedFilename.c_str(), FB::utf8_to_wstring(filename).c_str());

  // Notify the caller (from the browser) that we've finished saving the screenshot.
  std::vector<FB::variant> args;
  callback->InvokeAsync("", args);
}
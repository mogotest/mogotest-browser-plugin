/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/

#include "Logger.h"

#include "boost/format.hpp"

Logger::Logger(FB::BrowserHostPtr browser) : m_browser(browser)
{
}

Logger::~Logger()
{
}

void Logger::debug(const std::string message)
{
  return log("DEBUG", message);
}

void Logger::info(const std::string message)
{
  return log("INFO", message);
}

void Logger::error(const std::string message)
{
  return log("ERROR", message);
}

// Taken from MSDN: ms-help://MS.MSDNQTR.v80.en/MS.MSDN.v80/MS.WIN32COM.v10.en/debug/base/retrieving_the_last_error_code.htms
void Logger::win32Error(const std::string function)
{
  LPVOID lpMsgBuf;
  DWORD dw = GetLastError(); 

  FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER | 
    FORMAT_MESSAGE_FROM_SYSTEM,
    NULL,
    dw,
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
    (LPTSTR) &lpMsgBuf,
    0, NULL );

  error(str(boost::format("%s failed with error %d: %s") % function % dw % lpMsgBuf));

  LocalFree(lpMsgBuf);
}

void Logger::log(const std::string level, const std::string message)
{
  this->m_browser->htmlLog(str(boost::format("[%s] %s") % level % message));
}
#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for Mogotest Plugin
#
#\**********************************************************/

set(PLUGIN_NAME "MogotestPlugin")
set(PLUGIN_PREFIX "MOGO")
set(COMPANY_NAME "Mogoterra")

# ActiveX constants:
set(FBTYPELIB_NAME MogotestPluginLib)
set(FBTYPELIB_DESC "MogotestPlugin 1.0 Type Library")
set(IFBControl_DESC "MogotestPlugin Control Interface")
set(FBControl_DESC "MogotestPlugin Control Class")
set(IFBComJavascriptObject_DESC "MogotestPlugin IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "MogotestPlugin ComJavascriptObject Class")
set(IFBComEventSource_DESC "MogotestPlugin IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 0bad7aad-4392-5eec-8e30-7a1c9d690b1f)
set(IFBControl_GUID bd6045a3-8ff8-5559-93f2-29fb21073639)
set(FBControl_GUID 8216364c-898c-501a-b000-304827fe6f12)
set(IFBComJavascriptObject_GUID 1cfa18b7-0f43-57b3-93b5-544a7f51898f)
set(FBComJavascriptObject_GUID d776cb86-2e89-52e7-a794-e5f83e555f7e)
set(IFBComEventSource_GUID 945184dd-e596-5fbc-9c5c-cd9f80084e31)

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "Mogoterra.MogotestPlugin")
set(MOZILLA_PLUGINID "mogotest.com/MogotestPlugin")

# strings
set(FBSTRING_CompanyName "Mogoterra, Inc.")
set(FBSTRING_FileDescription "Browser plugin for Mogotest operations.")
set(FBSTRING_PLUGIN_VERSION "1.3.0.0")
set(FBSTRING_LegalCopyright "Copyright 2010 - 2012 Mogoterra, Inc.")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "Mogotest Plugin")
set(FBSTRING_FileExtents "")
set(FBSTRING_PluginName "Mogotest Plugin")
set(FBSTRING_MIMEType "application/x-mogotestplugin")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

#set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 1)
set(FBMAC_USE_COCOA 1)
set(FBMAC_USE_COREGRAPHICS 1)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)

add_firebreath_library(log4cplus)

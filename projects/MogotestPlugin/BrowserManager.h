/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/


#include "Logger.h"
#include "BrowserHost.h"
#include "win_common.h"

#ifndef H_BrowserManager
#define H_BrowserManager

/**
 * BrowserManager is a class for manipulating a Web browser's position and size on the Windows operating system.
 **/
class BrowserManager
{
public:
  BrowserManager(HWND browser, HWND canvas, FB::BrowserHostPtr host, Logger* logger);
  virtual ~BrowserManager();

  void maximize();
  void restore();

private:
  HWND m_browser;
  HWND m_canvas;
  RECT m_originalLocation;
  FB::BrowserHostPtr m_host;
  Logger* logger;
  bool m_isMaximized;

  void setupResizeHook();
  void refreshCanvas();
};

#endif
/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/


#include <string>

#include "win_common.h"
#include "BrowserHost.h"

#ifndef H_Logger
#define H_Logger

/**
 * Simple logging interface to the JavaScript console in the browser.
 **/
class Logger
{
public:
  Logger(FB::BrowserHostPtr browser);
  virtual ~Logger();

  void error(const std::string message);
  void debug(const std::string message);
  void info(const std::string message);
  void win32Error(const std::string function);

private:
  FB::BrowserHostPtr m_browser;

  void log(const std::string level, const std::string message);
};

#endif
/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/

#include "MogotestPluginAPI.h"

#include "MogotestPlugin.h"

///////////////////////////////////////////////////////////////////////////////
/// @fn MogotestPlugin::StaticInitialize()
///
/// @brief  Called from PluginFactory::globalPluginInitialize()
///
/// @see FB::FactoryBase::globalPluginInitialize
///////////////////////////////////////////////////////////////////////////////
void MogotestPlugin::StaticInitialize()
{
  // Place one-time initialization stuff here; As of FireBreath 1.4 this should only
  // be called once per process.
}

///////////////////////////////////////////////////////////////////////////////
/// @fn MogotestPlugin::StaticInitialize()
///
/// @brief  Called from PluginFactory::globalPluginDeinitialize()
///
/// @see FB::FactoryBase::globalPluginDeinitialize
///////////////////////////////////////////////////////////////////////////////
void MogotestPlugin::StaticDeinitialize()
{
  // Place one-time deinitialization stuff here. As of FireBreath 1.4 this should
  // always be called just before the plugin library is unloaded.
}

///////////////////////////////////////////////////////////////////////////////
/// @brief  MogotestPlugin constructor.  Note that your API is not available
///         at this point, nor the window.  For best results wait to use
///         the JSAPI object until the onPluginReady method is called
///////////////////////////////////////////////////////////////////////////////
MogotestPlugin::MogotestPlugin()
{
}

///////////////////////////////////////////////////////////////////////////////
/// @brief  MogotestPlugin destructor.
///////////////////////////////////////////////////////////////////////////////
MogotestPlugin::~MogotestPlugin()
{
  // This is optional, but if you reset m_api (the shared_ptr to your JSAPI
  // root object) and tell the host to free the retained JSAPI objects then
  // unless you are holding another shared_ptr reference to your JSAPI object
  // they will be released here.
  releaseRootJSAPI();
  m_host->freeRetainedObjects();
}

void MogotestPlugin::onPluginReady()
{
  // When this is called, the BrowserHost is attached, the JSAPI object is
  // created, and we are ready to interact with the page and such.  The
  // PluginWindow may or may not have already fire the AttachedEvent at
  // this point.
}

void MogotestPlugin::shutdown()
{
  // This will be called when it is time for the plugin to shut down;
  // any threads or anything else that may hold a shared_ptr to this
  // object should be released here so that this object can be safely
  // destroyed. This is the last point that shared_from_this and weak_ptr
  // references to this object will be valid
}

FB::JSAPIPtr MogotestPlugin::createJSAPI()
{
  // m_host is the BrowserHost
  return boost::make_shared<MogotestPluginAPI>(FB::ptr_cast<MogotestPlugin>(shared_from_this()), m_host);
}

bool MogotestPlugin::onMouseDown(FB::MouseDownEvent *evt, FB::PluginWindow *)
{
  //printf("Mouse down at: %d, %d\n", evt->m_x, evt->m_y);
  return false;
}

bool MogotestPlugin::onMouseUp(FB::MouseUpEvent *evt, FB::PluginWindow *)
{
  //printf("Mouse up at: %d, %d\n", evt->m_x, evt->m_y);
  return false;
}

bool MogotestPlugin::onMouseMove(FB::MouseMoveEvent *evt, FB::PluginWindow *)
{
  //printf("Mouse move at: %d, %d\n", evt->m_x, evt->m_y);
  return false;
}
bool MogotestPlugin::onWindowAttached(FB::AttachedEvent *evt, FB::PluginWindow *)
{
  boost::shared_ptr<MogotestPluginAPI> api(FB::ptr_cast<MogotestPluginAPI>(getRootJSAPI()));
  //api->setPlugin(FB::ptr_cast<MogotestPlugin>(shared_from_this()));

  return false;
}

bool MogotestPlugin::onWindowDetached(FB::DetachedEvent *evt, FB::PluginWindow *)
{
  boost::shared_ptr<MogotestPluginAPI> api(FB::ptr_cast<MogotestPluginAPI>(getRootJSAPI()));
  //api->setPlugin(NULL);

  return false;
}
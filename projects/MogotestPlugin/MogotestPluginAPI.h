/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "MogotestPlugin.h"
#include "Logger.h"
#include "win_common.h"
#include "NpapiBrowserHost.h"

#include "boost/format.hpp"

#ifndef H_MogotestPluginAPI
#define H_MogotestPluginAPI

/**
 * JavaScript interface for the MogotestPlugin.
 **/
class MogotestPluginAPI : public FB::JSAPIAuto
{
public:
  MogotestPluginAPI(const MogotestPluginPtr& plugin, const FB::BrowserHostPtr& host);
  virtual ~MogotestPluginAPI();

  MogotestPluginPtr getPlugin();

  // Read-only property ${PROPERTY.ident}
  std::string get_version();

  void captureEntirePageScreenshot(const FB::variant& filename, const FB::JSObjectPtr callback);
  void captureEntirePageScreenshotThread(const std::string filename, const std::string userAgent, const FB::JSObjectPtr callback);

private:
  MogotestPluginWeakPtr m_plugin;
  FB::BrowserHostPtr m_host;
  Logger* logger;

  std::string m_testString; 

  HWND getRenderWindow(const wchar_t* windowName);
  HWND getBrowserWindow();
};

#endif // H_MogotestPluginAPI

/**
 * Copyright 2010 Mogoterra, Inc.
 * All rights reserved.
 **/

#include "BrowserManager.h"
#include "DOM/Document.h"

#include "boost/format.hpp"

LRESULT CALLBACK MinMaxInfoHandler(HWND, UINT, WPARAM, LPARAM);

// Define a shared data segment.  Variables in this segment can be shared across processes that load this DLL.
#pragma data_seg("SHARED")
HHOOK nextHook = NULL;
int maxWidth = 0;
int maxHeight = 0;
#pragma data_seg()

#pragma comment(linker, "/section:SHARED,RWS")

// Microsoft linker helper for getting the DLL's HINSTANCE.
// See http://blogs.msdn.com/oldnewthing/archive/2004/10/25/247180.aspx for more details.
//
// If you need to link with a non-MS linker, you would have to add code to look up the DLL's
// path in HKCR.  regsvr32 stores the path under the CLSID key for the 'Snapsie.CoSnapsie' interface.
EXTERN_C IMAGE_DOS_HEADER __ImageBase;

BrowserManager::BrowserManager(HWND browser, HWND canvas, FB::BrowserHostPtr host, Logger* logger) : m_browser(browser), m_canvas(canvas), m_host(host), logger(logger)
{
}

BrowserManager::~BrowserManager()
{
}

void BrowserManager::maximize()
{
  RECT rcClient;
  RECT rcWindow;
  POINT chromeDifference;

  // The resize message is being ignored if the window appears to be maximized.  There's likely a
  // way to bypass that.  My ghetto way is to unmaximize the window, then move on with setting
  // the window to the dimensions we really want.  This is okay because we revert back to the
  // original dimensions afterward.
  //
  // Make sure to unmaximize before we get the browser dimensions because some browsers, such as
  // Google Chrome, actually change their browser chrome properties based upon whether the window
  // is maximized or not.
  m_isMaximized = IsZoomed(m_browser);
  if (m_isMaximized)
  {
    ShowWindow(m_browser, SW_RESTORE);
  }

  // Get the size of the of the browser and the render canvas.
  GetClientRect(m_canvas, &rcClient);
  GetWindowRect(m_browser, &rcWindow);

  // Figure out the size of the browser chrome.
  chromeDifference.x = (rcWindow.right - rcWindow.left) - rcClient.right;
  chromeDifference.y = (rcWindow.bottom - rcWindow.top) - rcClient.bottom;
  
  // Get the document dimensions with the current browser size.
  int browserWidth = this->m_host->getDOMDocument()->getBody()->getScrollWidth();
  int browserHeight = this->m_host->getDOMDocument()->getBody()->getScrollHeight();

  // Update the max heights that our resize hook will allow.
  maxWidth = browserWidth + chromeDifference.x;
  maxHeight = browserHeight + chromeDifference.y;

  setupResizeHook();

  logger->info("Maximizing");

  // First, change just the width, since this could affect the browser height.
  BOOL result = MoveWindow(m_browser, 0, 0, maxWidth, rcWindow.bottom - rcWindow.top, TRUE);
  if (FALSE == result)
  {
    logger->win32Error("MoveWindow");
  }
  refreshCanvas();

  // Now that we've changed the browser width, get the document height again.  The height may have changed as
  // width changed, due to element overflow.
  browserHeight = this->m_host->getDOMDocument()->getBody()->getScrollHeight();
  maxHeight = browserHeight + chromeDifference.y;

  // Resize the browser again, this time with the final dimensions.
  result = MoveWindow(m_browser, 0, 0, maxWidth, maxHeight, TRUE);
  if (FALSE == result)
  {
    logger->win32Error("MoveWindow");
  }
  refreshCanvas();

  // Restore the original window resize message handler.
  UnhookWindowsHookEx(nextHook);

  // Note the original window location so that we can restore it later.
  m_originalLocation = rcWindow;
}

void BrowserManager::restore()
{
  // Restore the browser to the original dimensions.
  if (m_isMaximized)
  {
    ShowWindow(m_browser, SW_MAXIMIZE);
  }
  else
  {
    int width =  m_originalLocation.right - m_originalLocation.left;
    int height = m_originalLocation.bottom - m_originalLocation.top;

    HRESULT result = MoveWindow(m_browser, m_originalLocation.left, m_originalLocation.top, width, height, TRUE);
    if (FAILED(result))
    {
      logger->win32Error("Restoring window.");
    }
  }
}

void BrowserManager::refreshCanvas()
{
  InvalidateRect(m_canvas, NULL, TRUE);
  UpdateWindow(m_canvas);
}

void BrowserManager::setupResizeHook()
{
  // Get the path to this DLL so we can load it up with LoadLibrary.
  TCHAR dllPath[_MAX_PATH];
  GetModuleFileName((HINSTANCE) &__ImageBase, dllPath, _MAX_PATH);

  // Get the path to the Windows hook we use to allow resizing the window greater than the virtual screen resolution.
  HINSTANCE hinstDLL = LoadLibrary(dllPath);
  HOOKPROC hkprcSysMsg = (HOOKPROC)GetProcAddress(hinstDLL, "CallWndProc");
  if (hkprcSysMsg == NULL)
  {
    logger->win32Error("GetProcAddress");
  }
  
  // Install the Windows hook.
  nextHook = SetWindowsHookEx(WH_CALLWNDPROC, hkprcSysMsg, hinstDLL, 0);
  if (nextHook == 0)
  {
    logger->win32Error("SetWindowsHookEx");
  }
}


// Many thanks to sunnyandy for helping out with this approach.  What we're doing here is setting up
// a Windows hook to see incoming messages to the IEFrame's message processor.  Once we find one that's
// WM_GETMINMAXINFO, we inject our own message processor into the IEFrame process to handle that one
// message.  WM_GETMINMAXINFO is sent on a resize event so the process can see how large a window can be.
// By modifying the max values, we can allow a window to be sized greater than the (virtual) screen resolution
// would otherwise allow.
//
// See the discussion here: http://www.codeguru.com/forum/showthread.php?p=1889928
LRESULT WINAPI CallWndProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    CWPSTRUCT* cwp = (CWPSTRUCT*) lParam;

    if (WM_GETMINMAXINFO == cwp->message)
    {
        // Inject our own message processor into the process so we can modify the WM_GETMINMAXINFO message.
        // It is not possible to modify the message from this hook, so the best we can do is inject a function that can.
        LONG_PTR proc = SetWindowLongPtr(cwp->hwnd, GWL_WNDPROC, (LONG_PTR) MinMaxInfoHandler);
        SetProp(cwp->hwnd, L"__original_message_processor__", (HANDLE) proc);
    }

    return CallNextHookEx(nextHook, nCode, wParam, lParam);
}

// This function is our message processor that we inject into the IEFrame process.  Its sole purpose
// is to process WM_GETMINMAXINFO messages and modify the max tracking size so that we can resize the
// IEFrame window to greater than the virtual screen resolution.  All other messages are delegated to
// the original IEFrame message processor.  This function uninjects itself immediately upon execution.
LRESULT CALLBACK MinMaxInfoHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    // Grab a reference to the original message processor.
    HANDLE originalMessageProc = GetProp(hwnd, L"__original_message_processor__");
    RemoveProp(hwnd, L"__original_message_processor__");

    // Uninject this method.
    SetWindowLongPtr(hwnd, GWL_WNDPROC, (LONG_PTR) originalMessageProc);

    if (WM_GETMINMAXINFO == message)
    {
        MINMAXINFO* minMaxInfo = (MINMAXINFO*) lParam;

		minMaxInfo->ptMaxTrackSize.x = maxWidth;
        minMaxInfo->ptMaxTrackSize.y = maxHeight;

        // We're not going to pass this message onto the original message processor, so we should
        // return 0, per the documentation for the WM_GETMINMAXINFO message.
        return 0;
    }

    // All other messages should be handled by the original message processor.
    return CallWindowProc((WNDPROC) originalMessageProc, hwnd, message, wParam, lParam);
}
